import http from "./http";

const apiUrl = "http://localhost:3900/api"+'/genres';

export function getGenres() {
  return http.get(apiUrl);
}
